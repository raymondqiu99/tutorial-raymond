import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
// import { GetAPIComponent } from './list/get-api/get-api.component';
// import { PostAPIComponent } from './list/post-api/post-api.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: 'list', component: ListComponent},
  // { path: 'table/getApi', component: GetAPIComponent},
  { path: 'table', component: TableComponent},
  // { path: 'list/postApi', component: PostAPIComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
