import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/api.service'; 

@Component({
  selector: 'app-get-api',
  templateUrl: './get-api.component.html',
  styleUrls: ['./get-api.component.scss']
})
export class GetAPIComponent {

  title = 'Get API Test';

  data$:Observable<any> | undefined;

  constructor(private ApiService:ApiService) {
  }

  getClick() {
    this.data$ = this.ApiService.getApiData();
  }
}
