import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { map, Subscription } from 'rxjs';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-post-api',
  templateUrl: './post-api.component.html',
  styleUrls: ['./post-api.component.scss']
})
export class PostAPIComponent implements OnDestroy{

  title = 'POST API Test';

  data$: Subscription | undefined;

  @Output() postData = new EventEmitter();

  constructor(private Api:ApiService) {
  }

  postClick() {
      this.data$ = this.Api.setApiData().pipe(
        map(data => JSON.stringify(data))
      ).subscribe(data => this.postData.emit(data));
  }

  ngOnDestroy(): void {
    this.data$?.unsubscribe();
  }
}
