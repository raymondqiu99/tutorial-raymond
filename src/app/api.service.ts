import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http : HttpClient,
    ) { }

   getApiData():Observable<any>{
    const url ='https://oocommapi-dev.aswatson.com/ai/v2/category/tag/wtctw/154214';
    const apiKey = '8n4pzh3oXofR4PCIUZvbrkXQPY3L58LgUBfIDEeEqlHXkj0n';
     return this.http.get(url,
       {
         headers:{
           'apiKey':apiKey
         }
       });
   }
   
   setApiData():Observable<any>{
     const url = 'https://oocommapi-dev.aswatson.com/ai/v2/tracking/wtchk/events';
     const body ={
       "value": {
           "type": "JSON",
           "data": {
               "eventId": "WTCHK_IMAGESEARCH_PRODUCT",
               "eventType": "CLICK_EVENT",
               "url": "{current_url}",
               "buCode": "pnshk",
               "recommendationId": "prodRecomm00001",
               "recommendedProductCode": "BP_10087",
               "referrerProductCode": "500035",
               "userId": "n/a",
               "sessionId": "92afda17-c18e-4b94-8956-eb173d44a524",
               "imageHash": "8A2E90BF9058C3C3",
               "clickedProduct":"{product code being clicked}"
           },
           "timestamp": "2022-12-30T02:47:23.333Z"
       }
   };
     return this.http.post(url,body);
   }
}
